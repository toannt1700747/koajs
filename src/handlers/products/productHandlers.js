import {
  getAll,
  getOne,
  getIndex,
  add,
  update,
  remove,
} from '../../database/products/productRepository';

export async function getProduct(ctx) {
  try {
    const id = ctx.params.id;

    const fields = ctx.query.fields;

    const index = getIndex(id);
    if (index === -1) {
      throw new Error('Product not found');
    }
    const product = getOne({ id, fields });

    return (ctx.body = {
      data: product,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}

export async function getProducts(ctx) {
  try {
    const limit = ctx.query.limit;
    const sort = ctx.query.sort;

    let products = getAll({ limit, sort });

    return (ctx.body = {
      data: products,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}

export async function addProduct(ctx) {
  try {
    const productData = ctx.request.body;

    add(productData);

    ctx.status = 201;
    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}

export async function updateProduct(ctx) {
  try {
    const productData = ctx.request.body;
    const { id } = ctx.params;
    const index = getIndex(id);

    if (index === -1) {
      throw new Error('Product not found');
    }

    update({ id, productData });

    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}

export async function removeProduct(ctx) {
  try {
    const { id } = ctx.params;
    const index_product = getIndex(id);

    if (index_product === -1) {
      throw new Error('Product not found');
    }

    remove(id);

    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}
