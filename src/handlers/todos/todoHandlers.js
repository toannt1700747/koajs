import {
  getAll,
  getOne,
  getIndex,
  add,
  update,
  updateMass,
  remove,
  removeMass,
} from '../../database/todos/todoRepository';

export async function getTodos(ctx) {
  try {
    const limit = ctx.query.limit;
    const sort = ctx.query.sort;
    const todos = getAll({ limit, sort });
    return (ctx.body = {
      data: todos,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      data: [],
      error: e.message,
    });
  }
}

export async function getTodo(ctx) {
  try {
    const { id } = ctx.params;
    const fields = ctx.query.fields;

    const index_todo = getIndex(id);
    if (index_todo === -1) {
      throw new Error('Todo not found');
    }

    const todo = getOne({ id, fields });
    return (ctx.body = {
      data: todo,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

export async function addTodo(ctx) {
  try {
    const postData = ctx.request.body;
    const data = add(postData);
    ctx.status = 201;
    return (ctx.body = {
      success: true,
      data,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

export async function updateTodo(ctx) {
  try {
    const { id } = ctx.params;

    const index_todo = getIndex(id);
    if (index_todo === -1) {
      throw new Error('Todo not found');
    }
    update(id);

    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      data: [],
      error: e.message,
    });
  }
}

export async function updateMassTodo(ctx) {
  try {
    const index_arr = ctx.request.body;

    updateMass(index_arr);
    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      data: [],
      error: e.message,
    });
  }
}

export async function removeTodo(ctx) {
  try {
    const { id } = ctx.params;
    const index_todo = getIndex(id);
    if (index_todo === -1) {
      throw new Error('Todo not found');
    }

    remove(id);

    ctx.body = {
      success: true,
    };
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    return (ctx.body = {
      success: false,
      error: e.message,
    });
  }
}

export async function removeMassTodo(ctx) {
  try {
    const indexArr = ctx.request.body;
    removeMass(indexArr);

    return (ctx.body = {
      success: true,
    });
  } catch (e) {
    console.error(e.message);
    ctx.status = 500;
    ctx.body = {
      success: false,
      data: [],
      error: e.message,
    };
  }
}
