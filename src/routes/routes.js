import Router from 'koa-router';
import * as productMiddle from '../middleware/productMiddleware';
import * as todoMiddle from '../middleware/todoMiddleware';
import * as productHandlers from '../handlers/products/productHandlers';
import * as todoHandlers from '../handlers/todos/todoHandlers';

const router = new Router({
  prefix: '/api',
});

// Products
// Add , Get all product,
router
  .post(
    '/products',
    productMiddle.productInputMiddleware,
    productHandlers.addProduct
  )
  .get('/products', productHandlers.getProducts);

// Get , update, delete product
router
  .get(
    '/product/:id',
    productMiddle.fieldInputMiddleware,
    productHandlers.getProduct
  )
  .put(
    '/product/:id',
    productMiddle.productInputMiddleware,
    productHandlers.updateProduct
  )
  .delete('/product/:id', productHandlers.removeProduct);

// Todos
// Add, get , update ,delete todos
router
  .post('/todos', todoMiddle.todoInputMiddleware, todoHandlers.addTodo)
  .get('/todos', todoHandlers.getTodos)
  .put('/todos', todoHandlers.updateMassTodo)
  .delete('/todos', todoHandlers.removeMassTodo);

// get, update , delete todo
router
  .get('/todo/:id', todoMiddle.todoFieldMiddleware, todoHandlers.getTodo)
  .put('/todo/:id', todoHandlers.updateTodo)
  .delete('/todo/:id', todoHandlers.removeTodo);

export default router;
