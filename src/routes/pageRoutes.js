import Router from 'koa-router';

import {
  getAll as getAllProducts,
  getOne as getOneProduct,
} from '../database/products/productRepository';

const router = new Router();

// Render List Product
router.get('/products', async (ctx) => {
  const products = getAllProducts();
  await ctx.render('pages/products', {
    products,
  });
});

// Render Product
router.get('/product/:id', async (ctx) => {
  const id = ctx.params.id;
  const product = getOneProduct(id);
  await ctx.render('pages/product', {
    product,
  });
});

export default router;
