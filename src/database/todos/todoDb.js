const { faker } = require('@faker-js/faker');
const fs = require('fs');

// Arrow function tạo product
const generateTodo = (id) => ({
  id,
  title: faker.lorem.words(2),
  complete: faker.datatype.boolean(false),
});

// Tạo mảng todos
const todos = [];

for (let i = 1; i <= 10; i++) {
  todos.push(generateTodo(i));
}

// Ghi file
fs.writeFileSync('todos.json', JSON.stringify({ data: todos }, null, 2));
