const fs = require('fs');
const { faker } = require('@faker-js/faker');
const { data: todos } = require('./todos.json');
import pick from '../../helpers/pick';

export function getAll({ limit = 10, sort = 'desc' } = {}) {
  let todoList = [...todos];
  if (sort) {
    todoList = todoList.sort((a, b) => {
      const dateA = new Date(a.createdAt);
      const dateB = new Date(b.createdAt);
      return sort == 'asc' ? dateA - dateB : dateB - dateA;
    });
  }
  if (limit) {
    todoList = todoList.slice(0, parseInt(limit));
  }

  return todoList;
}

export function getOne({ id, fields }) {
  const todo = todos.find((todo) => todo.id === parseInt(id));

  if (fields) {
    fields = fields.split(',');
    return pick(todo, fields);
  }
  return todo;
}

export function getIndex(id) {
  return todos.findIndex((todo) => todo.id === parseInt(id));
}

export function add(data) {
  const addTodo = {
    id: faker.number.int({ min: 100 }),
    ...data,
    complete: false,
  };
  const updateTodos = [addTodo, ...todos];
  fs.writeFileSync(
    './src/database/todos/todos.json',
    JSON.stringify(
      {
        data: updateTodos,
      },
      null,
      2
    )
  );
  return addTodo;
}

export function update(id) {
  // update products
  const updateTodos = todos.map((todo) => {
    if (todo.id === parseInt(id)) {
      return {
        ...todo,
        complete: true,
      };
    }
    return todo;
  });
  // add file
  return fs.writeFileSync(
    './src/database/todos/todos.json',
    JSON.stringify({ data: updateTodos }, null, 2)
  );
}

export function updateMass(index_arr = []) {
  // update products
  const updateTodos = todos.map((todo) => {
    if (index_arr.includes(todo.id)) {
      return {
        ...todo,
        complete: true,
      };
    }
    return todo;
  });
  // add file
  return fs.writeFileSync(
    './src/database/todos/todos.json',
    JSON.stringify({ data: updateTodos }, null, 2)
  );
}

export function remove(idDel) {
  const updateTodos = todos.filter(({ id }) => id != +parseInt(idDel));
  return fs.writeFileSync(
    './src/database/todos/todos.json',
    JSON.stringify(
      {
        data: updateTodos,
      },
      null,
      2
    )
  );
}

export function removeMass(indexArr = []) {
  const updateTodos = todos.filter((todo) => !indexArr.includes(todo.id));
  // add file
  return fs.writeFileSync(
    './src/database/todos/todos.json',
    JSON.stringify({ data: updateTodos }, null, 2)
  );
}
