import fs from 'fs';
const { faker } = require('@faker-js/faker');
const { data: products } = require('./products.json');
import pick from '../../helpers/pick';

export function getAll({ limit = 10, sort = 'desc' } = {}) {
  let productList = [...products];
  if (sort) {
    productList.sort((a, b) => {
      const dateA = new Date(a.createdAt);
      const dateB = new Date(b.createdAt);
      return sort == 'asc' ? dateA - dateB : dateB - dateA;
    });
  }
  if (limit) {
    productList = productList.slice(0, parseInt(limit));
  }

  return productList;
}

export function getOne({ id, fields }) {
  const product = products.find((product) => product.id === parseInt(id));

  if (fields) {
    fields = fields.split(',');
    console.log(fields);
    return pick(product, fields);
  }
  return product;
}

export function getIndex(id) {
  return products.findIndex((product) => product.id === parseInt(id));
}

export function add(product) {
  const addProduct = {
    id: faker.number.int({ min: 1500 }),
    ...product,
  };
  const updateProducts = [addProduct, ...products];
  return fs.writeFileSync(
    './src/database/products/products.json',
    JSON.stringify({ data: updateProducts }, null, 2)
  );
}

export function update({ id, productData }) {
  // update products
  const updateProducts = products.map((product) => {
    if (product.id === parseInt(id)) {
      return {
        ...product,
        ...productData,
      };
    }
    return product;
  });
  // add file
  return fs.writeFileSync(
    './src/database/products/products.json',
    JSON.stringify({ data: updateProducts }, null, 2)
  );
}

export function remove(idDel) {
  //remove product
  const updateProducts = products.filter(({ id }) => id !== parseInt(idDel));
  // add file
  return fs.writeFileSync(
    './src/database/products/products.json',
    JSON.stringify({ data: updateProducts }, null, 2)
  );
}
