const { faker } = require('@faker-js/faker');
const fs = require('fs');

// Arrow function tạo product
const generateProduct = (id) => ({
  id,
  name: faker.commerce.productName(),
  price: +faker.commerce.price({ dec: 0 }),
  description: faker.commerce.productDescription(),
  product: faker.commerce.product(),
  color: faker.color.human(),
  createdAt: faker.date.past(),
  image: faker.image.url(),
});

// Tạo mảng products
const products = [];

for (let i = 1; i <= 1000; i++) {
  products.push(generateProduct(i));
}

// Ghi file
fs.writeFileSync('products.json', JSON.stringify({ data: products }, null, 2));
