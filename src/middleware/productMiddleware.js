const yup = require('yup');

export async function productInputMiddleware(ctx, next) {
  try {
    const productData = ctx.request.body;
    let schema = yup.object().shape({
      name: yup.string().required(),
      price: yup.number().positive().integer().required(),
      description: yup.string().required(),
      product: yup.string().required(),
      color: yup.string().required(),
      createdAt: yup.date().default(() => new Date()),
      image: yup.string().url().required(),
    });

    await schema.validate(productData);
    return next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
}

export async function fieldInputMiddleware(ctx, next) {
  try {
    const fieldQuery = ctx.query.fields;
    if (fieldQuery) {
      let fields = fieldQuery.split(',');
      let schema = yup
        .array()
        .of(
          yup
            .mixed()
            .oneOf([
              'name',
              'price',
              'description',
              'product',
              'color',
              'createdAt',
              'image',
            ])
        );

      await schema.validate(fields);
    }

    return next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
}
