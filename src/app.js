import Koa from 'koa';
import cors from '@koa/cors';
import koaBody from 'koa-body';
import pageRoutes from './routes/pageRoutes.js';
import routes from './routes/routes.js';
import render from 'koa-ejs';
import path from 'path';

const app = new Koa();

render(app, {
  root: path.join(__dirname, 'views'),
  layout: 'layout/template',
  viewExt: 'html',
  cache: false,
  debug: true,
});

app.use(cors());
app.use(koaBody({ parsedMethods: ['GET', 'POST', 'PUT', 'PATCH', 'DELETE'] }));
app.use(pageRoutes.routes());
app.use(routes.routes());
app.use(routes.allowedMethods());

app.listen(8080, () => {
  console.log(`Server is on port 8080 !!!`);
});
